# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import Text, List
from dataclasses import dataclass

import copy
from geometry import Position



@dataclass(frozen = True, order = True)
class Arche:
    grille: List[List[Text]]

    def largeur(self):
        return len(self.grille[0])

    def hauteur(self):
        return len(self.grille)

    def case(self, position):
        if self.contient(position):
            return self.grille[position.ligne][position.colonne]
        return "0"

    def case_libre(self, position):
        return self.case(position) == "."

    def change_case(self, position, contenu):
        if self.contient(position):
            self.grille[position.ligne][position.colonne] = contenu

    def effface_case(position, self):
        self.change_case(position, ".")

    def contient(self, position):
        if 0 <= position.ligne < self.hauteur():
            if 0 <= position.colonne < self.largeur():
                return True
        return False

    def copie(self):
        return Arche(copy.deepcopy(self.grille))

    def affiche(self):
        for line in self.grille:
            for cell in line:
                print(cell.ljust(2, " "), end='')
            print("")


def new_arche():
    return Arche([
        ["0","0","0","0","0","0","0","0","0"],
        ["0","0",".",".",".",".",".","0","0"],
        ["0",".",".",".",".",".",".",".","0"],
        ["0",".",".",".",".",".",".",".","0"],
        ["0","0",".",".",".",".",".","0","0"],
        ["0","0","0","0","0","0","0","0","0"]])
