# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import Text, List
from dataclasses import dataclass

from geometry import Vecteur, Position


@dataclass(frozen = True, order = True)
class Animal:
    race: Text
    genre: int
    occupation: List[Vecteur]

    def identification(self):
        return self.race + str(self.genre)

    def place_occupe(self, position):
        return [position.addition(v) for v in self.occupation]


def animal(race, genre, vecteurs):
    return Animal(race, genre, [Vecteur(0, 0), *vecteurs])


def sont_voisins(animal1, position1, animal2, position2):
    place_animal2 = animal2.place_occupe(position2)
    for p in animal1.place_occupe(position1):
        for voisin in p.voisins():
            if voisin in place_animal2:
                return True
    return False


Lion1 = animal('L', 1, [Vecteur(0, 1)])
Lion2 = animal('L', 2, [Vecteur(0, 1), Vecteur(1, 0)])
Girafe1 = animal('G', 1, [Vecteur(0, 1), Vecteur(-1, 1)])
Girafe2 = animal('G', 2, [Vecteur(1, 0)])
Hippo1 = animal('H', 1, [Vecteur(0, 1)])
Hippo2 = animal('H', 2, [Vecteur(0, 1), Vecteur(0, 2)])
Zebre1 = animal('Z', 1, [Vecteur(0, 1)])
Zebre2 = animal('Z', 2, [Vecteur(1, 0)])
Elephant1 = animal('E', 1, [Vecteur(0, 1)])
Elephant2 = animal('E', 2, [Vecteur(0, 1), Vecteur(1, 1)])


Couples = [(Lion1, Lion2), (Girafe1, Girafe2), (Hippo1, Hippo2), (Zebre1, Zebre2), (Elephant1, Elephant2)]
