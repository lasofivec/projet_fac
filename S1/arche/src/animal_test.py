# -*- coding: utf-8 -*-
from __future__ import annotations

import pytest

from geometry import Position
from animal import Lion1, Lion2, sont_voisins


def test_place_occupe_animal():
    assert sorted(Lion2.place_occupe(Position(1, 3))) == sorted([Position(1, 3), Position(1, 4), Position(2, 3)])


def test_sont_voisins():
    assert sont_voisins(Lion1, Position(1, 2), Lion2, Position(3, 2)) == False
    assert sont_voisins(Lion1, Position(1, 2), Lion2, Position(2, 2)) == True
