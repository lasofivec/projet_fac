# -*- coding: utf-8 -*-
from __future__ import annotations

import pytest

from geometry import Vecteur, Position


def test_position_should_addition():
    assert Position(3, 5).addition(Vecteur(1, 0)) == Position(4, 5)
    assert Position(3, 5).addition(Vecteur(1, 1)) == Position(4, 6)


def test_position_should_have_voisins():
    assert sorted(Position(3, 5).voisins()) == sorted([Position(2, 5), Position(4, 5), Position(3, 4), Position(3, 6)])
    assert sorted(Position(0, 0).voisins()) == sorted([Position(1, 0), Position(-1, 0), Position(0, 1), Position(0, -1)])
