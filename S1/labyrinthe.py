import graph

lst = [[0, 1, 1, 0, 1, 1, 1, 0],
       [1, 0, 0, 1, 1, 0, 0, 1],
       [0, 1, 0, 1, 1, 0, 0, 1],
       [0, 1, 0, 1, 0, 0, 1, 1],
       [1, 0, 1, 0, 0, 1, 1, 0],
       [1, 1, 0, 0, 0, 1, 0, 1]]

def nb_colone(lstlst):
    return len(lstlst[0])

def nb_ligne(lstlst):
    return len(lstlst)

def carre(basecarreligne=0, basecarrecolone=0, taille=40):
    for ligne in range((basecarreligne*taille), (taille-1)+(basecarreligne*taille)):
        for colone in range((basecarrecolone*taille), (taille-1)+(basecarrecolone*taille)):
            graphe.plot(ligne, colone)



def dessin(lstlst):
    for y in range(nb_ligne(lstlst)):
        for x in range(nb_colone(lstlst)):
            if lstlst[y][x] == 0:
                carre(y, x)

graph.ouvre_fenetre(600, 800)
dessin(lst)
#carre(2, 2)
graph.attend_fenetre()
