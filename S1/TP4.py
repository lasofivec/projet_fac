import graph
import random
import pytest


def noir(haut,larg):
	for y in range(haut):
		for x in range(larg):
			graph.plot(y, x)


def bande_noire_gauche(haut,larg,larg_bande):
	for y in range(haut):
		for x in range(larg):
			if x < larg_bande:
				graph.plot(y, x)


def rectangle_noir(haut, larg, y1, y2, x1, x2):
	for y in range(haut):
		for x in range(larg):
			if x1 < x < x2 and y1 < y < y2:
				graph.plot(y, x)


def rectangle_blanc(haut, larg, y1, y2, x1, x2):
	for y in range(haut):
		for x in range(larg):
			if x1 > x or x > x2 or y1 > y or y > y2:
				graph.plot(y, x)


def damier(haut , larg, cote):
	for y in range(haut):
		d = y // (haut // cote)
		for x in range(larg):
			c = x // (larg // cote)
			if c % 2 == 0:
				if d % 2 == 0:
					graph.plot(y, x)
			else:
				if d % 2 != 0:
					graph.plot(y, x)


def test_to_square_count():
    assert to_square_count(400, 10) == 40
    assert to_square_count(405, 10) == 41

def to_square_count(full_size, square_size):
    if full_size % square_size == 0:
        return full_size // square_size
    return full_size // square_size + 1


def test_should_paint():
    assert should_paint(0, 0) == False
    assert should_paint(0, 1) == True
    assert should_paint(1, 0) == True
    assert should_paint(1, 1) == False

def should_paint(col, row):
    return (col + row) % 2 != 0


def test_to_square():
    assert to_square(0, 0, 100, 100, 50) == (0, 0, 50, 50)
    assert to_square(1, 1, 90, 90, 50) == (50, 50, 90, 90)


def to_square(col, row, larg, haut, cote):
    return (
        col * cote,
        row * cote,
        min((col + 1) * cote, larg),
        min((row + 1) * cote, haut)
    )


def draw_square(tlx, tly, brx, bry, coul):
    for x in range(tlx, brx):
        for y in range(tly, bry):
            graph.plot(y, x, coul)



def damier_couleur(haut , larg, cote):
    def choose_color_for(i, j):
        if should_paint(i, j):
            return random.choice(["black", "red", "green", "blue", "yellow", "cyan", "magenta",
            "orange", "darkgrey"])
        return "white"

    for i in range(to_square_count(larg, cote)):
        for j in range(to_square_count(haut, cote)):
            draw_square(*to_square(i, j, larg, haut, cote), choose_color_for(i, j))


def rayure_vertical(haut, larg ,cote):
	for y in range(haut):
		for x in range(larg):
			a = x // (larg // cote)
			if a % 2 == 0:
				graph.plot(y, x)



if __name__ == "__main__":
    graph.ouvre_fenetre(400,800)
    #noir(400,600)
    #bande_noire_gauche(400, 600, 100)
    #rectangle_noir(400,600,50,200,150,450)
    #rectangle_blanc(400,600,50,200,150,450)
    #rayure_vertical(400, 600, 50)
    #damier(400, 600, 50)
    damier_couleur(400, 800, 37)
    graph.attend_fenetre()


def map_bomber(haut , larg, cote):
	for y in range(haut):
		d = y // (haut // cote)
		for x in range(larg):
			c = x // (larg // cote)
			if c % 2 == 0 and d % 2 == 0:
				graph.plot(y, x)

